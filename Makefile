CC = g++
CFLAGS = -I.. -Wall -W -c
LFLAGS = -I.. -Wall -W -lm
LINK = -I/usr/X11/include -L/usr/X11/lib -lX11
BIN=bin/
OBJS = ImExtraction.o ImFilter.o Gaussian_Kernel.o main.o


all: $(OBJS)
	$(CC) $(LINK) $(LFLAGS) $(OBJS) -o run

ImExtraction.o : src/ImExtraction.cpp src/CImg.h src/ImExtraction.h src/rgb_struct.h
	$(CC) $(CFLAGS) src/ImExtraction.cpp

ImFilter.o : src/ImFilter.cpp src/ImFilter.h src/Gaussian_Kernel.h
	$(CC) $(CFLAGS) src/ImFilter.cpp

Gaussian_Kernel.o : src/Gaussian_Kernel.cpp src/Gaussian_Kernel.h
	$(CC) $(CFLAGS) src/Gaussian_Kernel.cpp

main.o : src/ImExtraction.h src/ImFilter.h src/rgb_struct.h src/CImg.h
	$(CC) $(CFLAGS) src/main.cpp

clean:
	rm *.o
