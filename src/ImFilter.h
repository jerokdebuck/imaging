

#ifndef IMFILTER_H
#define IMFILTER_H

using namespace std;

typedef unsigned char unchar;

class ImFilter {

    private:
        int height;
        int width;
        unchar ** matrix;
        unchar check_intensity(unchar);
        unchar check_intensity(int);
    public:
        ImFilter(unchar**, int, int);
        ~ImFilter();
        unchar** Gaussian_Smoothing(int,double);
        unchar** Moving_Average(int);
        unchar** Median(int);
        unchar** Unsharp_Masking(int, char*, double);
        unchar** High_Boost(double);
    
};






#endif //IMFILTER_H