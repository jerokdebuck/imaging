
#ifndef RGB_STRUCT
#define RGB_STRUCT

typedef unsigned char unchar;

typedef struct {
    unchar ** r_channel;
    unchar ** g_channel;
    unchar ** b_channel;
} rgb_struct;


#endif
