

#ifndef GAUSSIAN_KERNEL_H
#define GAUSSIAN_KERNEL_H
#include <cmath>

typedef unsigned char unchar;

class GKernel {
    
private:
    friend class ImFilter;
    unchar success;
    unchar kernel_dimension;
    double sigma;
    double ** kernel;
public:
    GKernel(unchar, double);
    ~GKernel();
    void print_kernel();
    
};

#endif // GAUSSIAN_KERNEL_H
