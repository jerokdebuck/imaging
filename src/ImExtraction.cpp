#include "CImg.h"
#include "ImExtraction.h"
#include "rgb_struct.h"

typedef unsigned char unchar;
using namespace cimg_library;


ImExtraction::ImExtraction(CImg<unchar> im, int height, int width, int is_bw) {
    this -> image = im;
    this -> height = height;
    this -> width = width;
    this -> is_bw = is_bw;
    
    this -> r_values = new unchar * [height];
    this -> g_values = new unchar * [height];
    this -> b_values = new unchar * [height];
    this -> rgb.r_channel = new unchar * [height];
    this -> rgb.g_channel = new unchar * [height];
    this -> rgb.b_channel = new unchar * [height];
    
    for (int i = 0; i < height; i++) {
        this -> r_values[i] = new unchar[width];
        this -> g_values[i] = new unchar[width];
        this -> b_values[i] = new unchar[width];
        this -> rgb.r_channel[i] = new unchar[width];
        this -> rgb.g_channel[i] = new unchar[width];
        this -> rgb.b_channel[i] = new unchar[width];
    }
    
    
}

ImExtraction::~ImExtraction() {
    for (int i = 0; i < height; i++) {
        delete[] r_values[i];
        delete[] g_values[i];
        delete[] b_values[i];
        delete[] rgb.r_channel[i];
        delete[] rgb.g_channel[i];
        delete[] rgb.b_channel[i];
    }
    delete[] r_values;
    delete[] g_values;
    delete[] b_values;
    delete[] rgb.r_channel;
    delete[] rgb.g_channel;
    delete[] rgb.b_channel;
    
}


void ImExtraction::extract() {
    if (!is_bw) {
        printf("Not bw\n");
        for (int i = 0; i < this -> height; i++) {
            for (int j = 0; j < this -> width; j++) {
                unchar * r = this -> image.data(i,j,0,0);
                unchar * g = this -> image.data(i,j,0,1);
                unchar * b = this -> image.data(i,j,0,2);
                r_values[i][j] = *r;
                g_values[i][j] = *g;
                b_values[i][j] = *b;
            }
            memcpy(rgb.r_channel[i], r_values[i], this->width);
            memcpy(rgb.g_channel[i], g_values[i], this->width);
            memcpy(rgb.b_channel[i], b_values[i], this->width);
        }
    } else{
        for (int i = 0; i < this -> height; i++) {
            for (int j = 0; j < this -> width; j++) {
                unchar * r = this -> image.data(i,j,0,0);
                unchar * g = this -> image.data(i,j,0,1);
                unchar * b = this -> image.data(i,j,0,1);
                unchar greyscale_intensity = unchar(round(*r*.299 + *g*.587 + *b*.0722));
                r_values[i][j] = greyscale_intensity;
                g_values[i][j] = greyscale_intensity;
                b_values[i][j] = greyscale_intensity;
            }
            memcpy(rgb.r_channel[i], r_values[i], this->width);
            memcpy(rgb.g_channel[i], g_values[i], this->width);
            memcpy(rgb.b_channel[i], g_values[i], this->width);
        }
    }
}

rgb_struct ImExtraction::return_rgb_matrix() {
    return this -> rgb;
}