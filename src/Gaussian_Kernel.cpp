#include <cmath>
#include "Gaussian_Kernel.h"
#include <stdio.h>

typedef unsigned char unchar;

GKernel::GKernel(unchar dimension, double sigma) {
    this -> success = 0;
    if (dimension % 2 == 0) {
        printf("The kernel dimension must be odd\n");
    } else {
        
        this -> kernel = new double * [dimension];
        for (int i = 0; i < dimension; i++) {
            this -> kernel[i] = new double[dimension];
        }
        
        this -> sigma = sigma;
        this -> kernel_dimension = dimension;
        
        double s = 2.0*sigma*sigma;
        double sum = 0;
        double r;
        
        int low = -1*( (dimension-1) / 2);
        int high = (dimension-1) / 2;
        
        for (int i = low; i <= high; i++) {
            for (int j = low; j <= high; j++) {
                r = sqrt(i*i + j*j);
                this -> kernel[i+high][j+high] = (exp(-(r*r)/s))/(M_PI * s);
                sum += this -> kernel[i+high][j+high];
                
            }
        }
        
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                this->kernel[i][j] /= sum;
            }
        }
        this -> success = 1;
    }

}

GKernel::~GKernel() {
    if (this->success) {
        for (int i = 0; i < this -> kernel_dimension; i++) {
            delete[] this -> kernel[i];
        }
        delete[] this->kernel;
    }
}

void GKernel::print_kernel() {
    if (!this->success) {
        printf("Could not print, kernel not created");
    } else {
        printf("%dx%d Gaussian Smoothing Kernel with sigma %f\n", this->kernel_dimension, this->kernel_dimension, this->sigma);
        for (int i = 0; i < this->kernel_dimension; i++) {
            for (int j = 0; j < this->kernel_dimension; j++) {
                printf("%f\t", this->kernel[i][j]);
            }
            printf("\n");
        }
        
    }
    
}




