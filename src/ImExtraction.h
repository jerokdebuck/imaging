#include "CImg.h"
#include "rgb_struct.h"

#ifndef ImExtraction_H_
#define ImExtraction_H_

typedef unsigned char unchar;
using namespace cimg_library;


class ImExtraction {
    
    
private:
    int height;
    int width;
    int is_bw;
    CImg<unchar> image;
    unchar ** r_values;
    unchar ** g_values;
    unchar ** b_values;
    rgb_struct rgb;
    
public:
    ImExtraction(CImg<unchar>, int, int, int);
    ~ImExtraction();
    void extract();
    rgb_struct return_rgb_matrix();

    
};


#endif