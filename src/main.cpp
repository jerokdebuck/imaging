//
//  main.cpp
//  processing
//
//  Created by darian hadjiabadi on 7/30/15.
//  Copyright (c) 2015 darian hadjiabadi. All rights reserved.
//

#include <iostream>
#include <stdio.h>
//#include "ImageIO.h"
#include "ImExtraction.h"
#include "ImFilter.h"
#include "CImg.h"
#include "rgb_struct.h"

#define COMPONENTS 3

typedef unsigned char unchar;
using namespace cimg_library;
//using namespace Magick;


int main(int argc, const char * argv[]) {

    char * lp_filters[] = {"Gaussian", "Median", "Average", "Fail"};
    
    
    CImg<unchar> image("frequency.png");
    int width = image.width();
    int height = image.height();
    int is_bw = 0;
    ImExtraction imex(image, height, width, is_bw);
    imex.extract();
    
    
    rgb_struct rgb = imex.return_rgb_matrix();
    
    /*for (int i = 0; i < 50; i++) {
        for (int j = 0; j < 50; j++) {
            printf("R:%d, G:%d, B:%d|", rgb.r_channel[i][j], rgb.g_channel[i][j], rgb.b_channel[i][j]);
        }
        printf("\n");
    } */
    
    
    rgb_struct filtered_rgb;
    rgb_struct difference_rgb;
    filtered_rgb.r_channel = new unchar * [height];
    filtered_rgb.g_channel = new unchar * [height];
    filtered_rgb.b_channel = new unchar * [height];
    difference_rgb.r_channel = new unchar * [height];
    difference_rgb.g_channel = new unchar * [height];
    difference_rgb.b_channel = new unchar * [height];
    for (int i = 0; i < height; i++) {
        filtered_rgb.r_channel[i] = new unchar[width];
        filtered_rgb.g_channel[i] = new unchar[width];
        filtered_rgb.b_channel[i] = new unchar[width];
        difference_rgb.r_channel[i] = new unchar[width];
        difference_rgb.g_channel[i] = new unchar[width];
        difference_rgb.b_channel[i] = new unchar[width];
    }
    
    ImFilter r_filter(rgb.r_channel, height, width);
    ImFilter g_filter(rgb.g_channel, height, width);
    ImFilter b_filter(rgb.b_channel, height, width);
    
    
    // Uncomment a set of these to do filtering
    
    /*unchar ** r_filtered = r_filter.Gaussian_Smoothing(7,10.0);
    unchar ** g_filtered = g_filter.Gaussian_Smoothing(7,10.0);
    unchar ** b_filtered = b_filter.Gaussian_Smoothing(7,10.0); */
    
    /*unchar ** r_filtered = r_filter.Moving_Average(5);
    unchar ** g_filtered = g_filter.Moving_Average(5);
    unchar ** b_filtered = g_filter.Moving_Average(5); */
    
    /*unchar ** r_filtered = r_filter.Median(3);
    unchar ** g_filtered = g_filter.Median(3);
    unchar ** b_filtered = b_filter.Median(3); */
    
    /*unchar ** r_filtered = r_filter.Unsharp_Masking(5, lp_filters[0], 1.0);
    unchar ** g_filtered = g_filter.Unsharp_Masking(5, lp_filters[0], 1.0);
    unchar ** b_filtered = b_filter.Unsharp_Masking(5, lp_filters[0], 3.0); */
    
    /*unchar ** r_filtered = r_filter.High_Boost(1.0);
    unchar ** g_filtered = g_filter.High_Boost(1.0);
    unchar ** b_filtered = b_filter.High_Boost(1.0); */
    
    for (int i = 0; i < height; i++) {
        memcpy(filtered_rgb.r_channel[i],r_filtered[i],width);
        memcpy(filtered_rgb.g_channel[i],g_filtered[i], width);
        memcpy(filtered_rgb.b_channel[i],b_filtered[i], width);
    }

    // for now filtered image is actually a difference image just to take a look at what is being cut out
    CImg<unchar> filtered_image(width,height,1,3,255), visu(width,height,1,3,0);
    CImg<unchar> difference_image(width,height,1,3,255), visu2(500,400,1,3,0);
    for (int i = 0 ; i < height; i++) {
        for (int j = 0; j < width; j++) {
            difference_image(i,j,0,0) = abs(filtered_rgb.r_channel[i][j]-rgb.r_channel[i][j]);
            difference_image(i,j,0,1) = abs(filtered_rgb.g_channel[i][j]-rgb.g_channel[i][j]);
            difference_image(i,j,0,2) = abs(filtered_rgb.b_channel[i][j]-rgb.b_channel[i][j]);
            filtered_image(i,j,0,0) = filtered_rgb.r_channel[i][j];
            filtered_image(i,j,0,1) = filtered_rgb.g_channel[i][j];
            filtered_image(i,j,0,2) = filtered_rgb.b_channel[i][j];
        }
    }

    
    //filtered_image.save("filter_output2.png");
    //difference_image.save("difference_output2.png");
    
    const unsigned char red[] = {255,0,0}, green[] = {0,255,0}, blue[] = {0,0,255};
    CImgDisplay main_disp(filtered_image, "Filtered Image"), draw_disp(visu, "Filtered Intensity profile");
    //CImgDisplay main_disp2(image, "Original Image");
    while ((!main_disp.is_closed() && !draw_disp.is_closed())) {
        main_disp.wait();
        if (main_disp.button() && main_disp.mouse_y()>=0) {
            const int y =  main_disp.mouse_y();
            visu.fill(0).draw_graph(filtered_image.get_crop(0,y,0,0,filtered_image.width()-1,y,0,0),red,1,1,0,255,0);
            visu.draw_graph(filtered_image.get_crop(0,y,0,1,filtered_image.width()-1,y,0,1),green,1,1,0,255,0);
            visu.draw_graph(filtered_image.get_crop(0,y,0,2,filtered_image.width()-1,y,0,2),blue,1,1,0,255,0).display(draw_disp);
        }
 
    }
    return 0;
}



