
#include <string.h>
#include <stdio.h>
#include <vector>
#include <cmath>
#include <cstdlib>
#include "ImFilter.h"
#include "Gaussian_Kernel.h"

#define BLACK 0
#define WHITE 255

typedef unsigned char unchar;
using namespace std;

ImFilter::ImFilter(unchar ** m, int height, int width) {
    this->matrix = new unchar* [height];
    for (int i = 0; i < height; i++) {
        this->matrix[i] = new unchar[width];
        memcpy(reinterpret_cast<char*>(this->matrix[i]), reinterpret_cast<char*>(m[i]), width);
    }
    
    this->height = height;
    this->width = width;
    
}

ImFilter::~ImFilter() {
    for (int i = 0; i < this->height; i++) {
        delete[] this->matrix[i];
    }
    delete[] this->matrix;
    
}

unchar** ImFilter::Gaussian_Smoothing(int dimension, double sigma) {
    unchar ** filtered;
    filtered = new unchar* [this->height];
    for (int i = 0; i < this->height; i++) {
        filtered[i] = new unchar[width];
        memcpy(reinterpret_cast<char*>(filtered[i]), reinterpret_cast<char*>(this->matrix[i]), width);
    }
    GKernel gkernel(dimension, sigma);
    //gkernel.print_kernel();
    double convolved_sum;
    
    int corner_pad = (dimension-1)/2;
    for (int i = corner_pad; i < height - corner_pad; i++) {
        for (int j = corner_pad; j < width - corner_pad; j++) {
            convolved_sum = 0.0;
            for (int x = -1*corner_pad; x <= corner_pad; x++) {
                for (int y = -1*corner_pad; y <= corner_pad; y++) {
                    convolved_sum += gkernel.kernel[x+corner_pad][y+corner_pad]*this->matrix[i-x][j+y];
                }
            }
            int char_sum = int(round(convolved_sum));
            filtered[i][j] = check_intensity(char_sum);
        }
    }
    
    return filtered;
}

unchar** ImFilter::Moving_Average(int dimension) {
    if (dimension % 2 == 0) {
        printf("Dimension must be odd, returning NULL\n");
        return NULL;
    }
    
    unchar ** filtered;
    filtered = new unchar* [this->height];
    for (int i = 0; i < this->height; i++) {
        filtered[i] = new unchar[width];
        memcpy(reinterpret_cast<char*>(filtered[i]), reinterpret_cast<char*>(this->matrix[i]), width);
    }
    int corner_pad = (dimension-1)/2;
    double convolved_sum = 0.0;
    
    for (int i = corner_pad; i < height - corner_pad; i++) {
        for (int j = corner_pad; j < width - corner_pad; j++) {
            convolved_sum = 0.0;
            for (int x = -1*corner_pad; x <= corner_pad; x++) {
                for (int y = -1*corner_pad; y <= corner_pad; y++) {
                    convolved_sum += this->matrix[i-x][j+y];
                }
            }
            convolved_sum /= (dimension*dimension);
            int char_sum = int(round(convolved_sum));
            filtered[i][j] = check_intensity(char_sum);
        }
        
    }
    return filtered;
}


unchar** ImFilter::Median(int dimension) {
    if (dimension % 2 == 0) {
        printf("Dimenson must be odd, returning NULL\n");
        return NULL;
    }
    unchar ** filtered;
    filtered = new unchar* [this->height];
    for (int i = 0; i < this->height;i++) {
        filtered[i] = new unchar[width];
        memcpy(reinterpret_cast<char*>(filtered[i]), reinterpret_cast<char*>(this->matrix[i]),width);
    }
    int corner_pad = (dimension-1)/2;
    for (int i = corner_pad; i < height - corner_pad; i++) {
        for (int j = corner_pad; j < width-corner_pad; j++) {
            unchar current_intensities[dimension*dimension];
            for (int x = -1*corner_pad; x <= corner_pad; x++) {
                for (int y = -1*corner_pad; y<=corner_pad; y++) {
                    current_intensities[(x+corner_pad)*dimension+(y+corner_pad)] = this->matrix[i-x][j+y];
                }
            }
            vector<unchar> pixel_vector(current_intensities, current_intensities+(dimension*dimension));
            sort(pixel_vector.begin(), pixel_vector.end());
            if (pixel_vector.size() % 2 == 1) {
                filtered[i][j] = check_intensity(pixel_vector.at((pixel_vector.size()/2)));
            } else {
                unchar x = pixel_vector.at((pixel_vector.size()/2));
                unchar y = pixel_vector.at((pixel_vector.size()/2) -1);
                unchar val = unchar(round((x+y)/2));
                filtered[i][j] = check_intensity(val);
            }
        }
    }
    return filtered;
}

unchar ** ImFilter::Unsharp_Masking(int dimension, char* low_pass, double sigma) {
    printf("%s\n", low_pass);
    unchar ** filtered;
    filtered = new unchar* [this->height];
    for (int i = 0; i < this->height; i++) {
        filtered[i] = new unchar[width];
        memcpy(reinterpret_cast<char*>(filtered[i]), reinterpret_cast<char*>(this->matrix[i]), width);
    }
    if (strcmp("Gaussian", low_pass) == 0) {
    
        unchar ** lpf = Gaussian_Smoothing(dimension, sigma);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                //filtered[i][j] = check_intensity((int)round(lpf[i][j]-this->matrix[i][j]));
                filtered[i][j] = check_intensity(abs(lpf[i][j]-this->matrix[i][j]));
            }
        }
        return filtered;
    } else if (strcmp("Average", low_pass) == 0) {
        unchar ** lpf = Moving_Average(dimension);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                filtered[i][j] = check_intensity((int)round(lpf[i][j]-this->matrix[i][j]));
                //filtered[i][j] = check_intensity(abs(lpf[i][j] - this->matrix[i][j]));
            }
        }
        return filtered;
    } else if (strcmp("Median", low_pass) == 0) {
        unchar ** lpf = Median(dimension);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                //filtered[i][j] = check_intensity(abs(lpf[i][j] - this->matrix[i][j]));
                filtered[i][j] = check_intensity((int)round(lpf[i][j]-this->matrix[i][j]));
            }
        }
        return filtered;
    }
    
    else {
        printf("No match\n");
    }
    return NULL;
}

// Uses Gaussian Smoothing Kernel as low pass, then scales it by scaled
unchar ** ImFilter::High_Boost(double scaled) {
    if (scaled < 1.0) {
        printf("Original Image must be scaled by a value >= 1. Returning null.\n");
        return NULL;
    }
    
    #define DIMENSION 3
    unchar ** filtered;
    filtered = new unchar*[this->height];
    for (int i = 0; i < this->height; i++) {
        filtered[i] = new unchar[width];
        memcpy(reinterpret_cast<char*>(filtered[i]), reinterpret_cast<char*>(this->matrix[i]),this->width);
    }
    double convolved_sum;
    int corner_pad = (DIMENSION-1)/2;
    
    for (int i = corner_pad; i < height-corner_pad; i++) {
        for (int j = corner_pad; j < width-corner_pad; j++) {
            
            
            double apkernel[DIMENSION][DIMENSION] = { {0.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 0.0} };
            double whole_kernel[DIMENSION][DIMENSION];
            GKernel lpkernel(DIMENSION,1.0);
            for (int i = 0; i < DIMENSION; i++) {
                for (int j = 0; j < DIMENSION; j++) {
                    apkernel[i][j] *= scaled;
                    whole_kernel[i][j] = (apkernel[i][j]-lpkernel.kernel[i][j]);
                }
            }
            convolved_sum = 0.0;
            for (int x = -1*corner_pad; x <=corner_pad; x++) {
                for (int y =-1*corner_pad; y<=corner_pad;y++) {
                    convolved_sum += whole_kernel[x+corner_pad][y+corner_pad]*this->matrix[i-x][j+y];
                }
            }
            int sum = (int) round(convolved_sum);
            filtered[i][j] = check_intensity(sum);
        }
    }
    return filtered;
    
    
}

unchar ImFilter::check_intensity(unchar intensity) {
    if (intensity < 0) {
        intensity = BLACK;
    } else if (intensity > 255) {
        intensity = WHITE;
    }
    return intensity;
}

unchar ImFilter::check_intensity(int intensity) {
    unchar new_intensity;
    if (intensity < 0) {
        new_intensity = 0;
    } else if (intensity > 255) {
        new_intensity = 255;
    } else {
        new_intensity = (unchar) intensity;
    }
    return new_intensity;
}


