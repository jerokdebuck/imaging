// ImageIO.h
// Implentation file for reading an image

#ifndef IMAGEIO_H
#define IMAGEIO_H

#include <fstream> // for file I/O

using namespace std;
typedef unsigned char unchar;


class ImageIO {
    public:
        ImageIO(const char*, int, int);
        ~ImageIO();
        unchar** return_matrix();
    private:
        int height;
        int width;
        ifstream* p_in_file;
        unchar image_header_data[1078];
        unchar ** input_matrix;
    
};



#endif //IMAGEIO_H

