// ImageIO.cpp

#include "ImageIO.h"
#include <fstream>
#include <string.h>
using namespace std;
typedef unsigned char unchar;

ImageIO::ImageIO(const char* file_name, int height, int width) {

    input_matrix = new unchar* [height];
    for (int i = 0; i < height; i++) {
        input_matrix[i] = new unchar[width];
    }
    p_in_file = new ifstream;
    p_in_file->open(file_name, ios::in | ios::binary);
    p_in_file->seekg(0, ios::beg);
    p_in_file->read(reinterpret_cast<char*>(image_header_data),1078);
    
    for (int i = 0; i < height; i++) {
        p_in_file->read(reinterpret_cast<char*>(input_matrix[i]), width);
    }
    p_in_file->close();
    
    this->height = height;
    this->width = width;

}

ImageIO::~ImageIO() {
    delete p_in_file;
    for (int i = 0; i < this->height; i++) {
        delete[] input_matrix[i];
    }
    delete[] input_matrix;
}

unchar** ImageIO::return_matrix() {
    return this->input_matrix;
}



